## Igthorn RethinkDB Gateway

[![Igthorn](http://igthorn.com/assets/images/igthorn_resized.png "Igthorn")](http://igthorn.com "Igthorn")

RethinkDB gateway allows CRUD on RethinkDB.

[![Build Status](https://gitlab.com/igthorn/rethinkdb-gateway/badges/develop/build.svg)](https://gitlab.com/igthorn/rethinkdb-gateway/builds)

Igthorn RethinkDB Gateway is a package that provides gateway class to run CRUD operations on RethinkDB using igthorn-request(to query) and igthorn-entity(to save, update and replace). It's part of [Igthorn](http://igthorn.com "Igthorn") project

### Usage

Typical usage would be to query for data using igthorn-request middleware which would be attached to every GET route of application(route would receive Request object(ightorn-request) as a part of request object and pass forward to gateway - class extending rethinkDbGateway from this package and defining 'relations'). Below, how it works in background:


```
  const sampleRequestData = {
    "joins": [
      "role"
    ],
    "fields": {
      "u.firstName": "userFirstName",
      "r.name": "role"
    }
  };

  var request = new Request();
  request.inflate(sampleRequestData);

  rethinkDbUserGateway.fetchAll(request)
    .then(function(results) {

      // console.log(results) - query result

    }).catch(function(error) {
      done(error);
    });

```

### Methods

Request class implements following methods:

* fetchAll(request:IghtornRequest) : promise

## To do

* Support different types of joins

* Refactor querying to subclass and move code to ES6 (const, let)

## License

[MIT](https://gitlab.com/igthorn/rethinkdb-gateway/blob/develop/license.md)
