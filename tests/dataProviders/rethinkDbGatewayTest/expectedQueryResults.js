
"use strict";

const _ = require("underscore");

const user1 ={
  id: "b808972a-ec28-4d69-a194-d1e05c5ff0e8",
  username: "mina.hernadi",
  firstName: "Mina",
  lastName: "Hernadi",
  age: 20,
  tags: [
    "da2f64bd-1d70-4415-a232-95b2f35366ba",
    "3f2213f2-0c04-44bf-907a-9f167404adff",
  ],
  roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
  createdAt: new Date("2015-08-22T22:24:15.000Z"),
  updatedAt: null
};

const user2 = {
  id: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
  username: "horgan.madore",
  firstName: "Horgan",
  lastName: "Madore",
  age: 26,
  tags: [
    "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8",
    "a239fb8d-a632-4f6e-91b1-16cbaa519800",
  ],
  roleId: "6eeac08b-818b-480f-896f-8e4ea3f57267",
  createdAt: new Date("2015-08-22T07:42:47.000Z"),
  updatedAt: null
};

const user3 = {
  id: "d7885ec6-ece3-427a-b15c-c114d27d2f82",
  username: "jerrie.mcginnis",
  firstName: "Jerrie",
  lastName: "Mcginnis",
  age: 23,
  tags: [
    "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8",
    "72db1cef-4e21-4946-a16e-1e0690d69dc7",
  ],
  roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
  createdAt: new Date("2015-08-22T07:34:11.000Z"),
  updatedAt: null
};

const user4 = {
  id: "4c374646-ff38-4a8b-adf6-0bf0e3723c75",
  username: "kristy.daniel",
  firstName: "Kristy",
  lastName: "Daniel",
  age: 17,
  tags: [],
  roleId: "6eeac08b-818b-480f-896f-8e4ea3f57267",
  createdAt: new Date("2015-08-22T05:34:12.000Z"),
  updatedAt: null
};

const user5 = {
  id: "ede8e906-03f4-40e8-9b8e-46c1aad72de6",
  username: "jardetzky.gallucio",
  firstName: "Jardetzky",
  lastName: "Gallucio",
  age: 35,
  tags: [
    "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71",
  ],
  roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
  createdAt: new Date("2015-08-22T07:34:11.000Z"),
  updatedAt: null
};

const role1 = {
  id: "6eeac08b-818b-480f-896f-8e4ea3f57267",
  name: "admin",
  description: "Application administrator",
  parent: null,
  status: "active",
  createdAt: new Date("2015-08-22T23:12:31.000Z"),
  updatedAt: null
};

const role2 = {
  id: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
  name: "user",
  description: "Application user",
  parent: "d16db438-35a9-4dcc-946c-c2460aa7dda7",
  status: "active",
  createdAt: new Date("2015-08-22T12:17:02.000Z"),
  updatedAt: null
};

const roles = {
  [role1.id]: role1,
  [role2.id]: role2
};

module.exports = {
  "all": [
    user1,
    user2,
    user3,
    user4,
    user5
  ],
  "limitedFields": [
    {
      id: user1.id,
      firstName: user1.firstName
    },
    {
      id: user2.id,
      firstName: user2.firstName
    },
    {
      id: user3.id,
      firstName: user3.firstName
    },
    {
      id: user4.id,
      firstName: user4.firstName
    },
    {
      id: user5.id,
      firstName: user5.firstName
    }
  ],
  "aliasedFields": [
    {
      userId: user1.id,
      userFirstName: user1.firstName
    },
    {
      userId: user2.id,
      userFirstName: user2.firstName
    },
    {
      userId: user3.id,
      userFirstName: user3.firstName
    },
    {
      userId: user4.id,
      userFirstName: user4.firstName
    },
    {
      userId: user5.id,
      userFirstName: user5.firstName
    }
  ],
  "joinedWithRole": [
    {
      userFirstName: user1.firstName,
      role: roles[user1.roleId].name,
    },
    {
      userFirstName: user2.firstName,
      role: roles[user2.roleId].name,
    },
    {
      userFirstName: user3.firstName,
      role: roles[user3.roleId].name,
    },
    {
      userFirstName: user4.firstName,
      role: roles[user4.roleId].name,
    },
    {
      userFirstName: user5.firstName,
      role: roles[user5.roleId].name,
    }
  ],
  "joinedWithRoleAllUserFields": [
    _.extend(_.clone(user1), {name: roles[user1.roleId].name}),
    _.extend(_.clone(user2), {name: roles[user2.roleId].name}),
    _.extend(_.clone(user3), {name: roles[user3.roleId].name}),
    _.extend(_.clone(user4), {name: roles[user4.roleId].name}),
    _.extend(_.clone(user5), {name: roles[user5.roleId].name})
  ],
  "userFirstNameJoinedWithAllRoleFields": [
    _.extend(_.clone(roles[user1.roleId]), {firstName: user1.firstName}),
    _.extend(_.clone(roles[user2.roleId]), {firstName: user2.firstName}),
    _.extend(_.clone(roles[user3.roleId]), {firstName: user3.firstName}),
    _.extend(_.clone(roles[user4.roleId]), {firstName: user4.firstName}),
    _.extend(_.clone(roles[user5.roleId]), {firstName: user5.firstName})
  ],
  "joinedWithCreatorAndAssignee": [
    {
      userFirstName: user1.firstName,
      creatorUsername: "unknown",
      assigneeUsername: "unknown"
    },
    {
      userFirstName: user2.firstName,
      creatorUsername: "kristy.daniel",
      assigneeUsername: "jardetzky.gallucio"
    },
    {
      userFirstName: user3.firstName,
      creatorUsername: "becky.hawkins",
      assigneeUsername: "biff.wellington"
    },
    {
      userFirstName: user4.firstName,
      creatorUsername: "lura.picard",
      assigneeUsername: "rigoberto.rubin"
    },
    {
      userFirstName: user5.firstName,
      creatorUsername: "jerrie.mcginnis",
      assigneeUsername: "horgan.madore"
    }
  ],
  "ticketsInId": [
    {
      userFirstName: user3.name
    },
    {
      userFirstName: user4.name
    }
  ],
  "ticketsNinId": [
    {
      userFirstName: user1.name
    },
    {
      userFirstName: user2.name
    },
    {
      userFirstName: user5.name
    }
  ],
  "ticketsComplexConditions": [
    {
      userFirstName: user2.name
    },
    {
      userFirstName: user5.name
    }
  ],
  "ticketsOrderedByNameAsc": [
    {
      userFirstName: user4.name
    },
    {
      userFirstName: user5.name
    },
    {
      userFirstName: user1.name
    },
    {
      userFirstName: user3.name
    },
    {
      userFirstName: user2.name
    }
  ],
  "ticketsOrderedByNameDesc": [
    {
      userFirstName: user2.name
    },
    {
      userFirstName: user3.name
    },
    {
      userFirstName: user1.name
    },
    {
      userFirstName: user5.name
    },
    {
      userFirstName: user4.name
    }
  ],
  "ticketsOrderedByMultiAsc": [
    {
      name: user5.firstName,
      storyPoints: user5.storyPoints,
      status: user5.status
    },
    {
      name: user3.firstName,
      storyPoints: user3.storyPoints,
      status: user3.status
    },
    {
      name: user1.firstName,
      storyPoints: user1.storyPoints,
      status: user1.status
    },
    {
      name: user2.firstName,
      storyPoints: user2.storyPoints,
      status: user2.status
    },
    {
      name: user4.firstName,
      storyPoints: user4.storyPoints,
      status: user4.status
    }
  ],
  "ticketsOrderedByMultiDesc": [
    {
      name: user4.firstName,
      storyPoints: user4.storyPoints,
      status: user4.status
    },
    {
      name: user2.firstName,
      storyPoints: user2.storyPoints,
      status: user2.status
    },
    {
      name: user1.firstName,
      storyPoints: user1.storyPoints,
      status: user1.status
    },
    {
      name: user3.firstName,
      storyPoints: user3.storyPoints,
      status: user3.status
    },
    {
      name: user5.firstName,
      storyPoints: user5.storyPoints,
      status: user5.status
    }
  ],
  "ticketsOrderedByMultiMixed": [
    {
      name: user4.firstName,
      storyPoints: user4.storyPoints,
      status: user4.status
    },
    {
      name: user2.firstName,
      storyPoints: user2.storyPoints,
      status: user2.status
    },
    {
      name: user5.firstName,
      storyPoints: user5.storyPoints,
      status: user5.status
    },
    {
      name: user3.firstName,
      storyPoints: user3.storyPoints,
      status: user3.status
    },
    {
      name: user1.firstName,
      storyPoints: user1.storyPoints,
      status: user1.status
    }
  ],
  "ticketsQueryLevel1Results": [
    {
      assigneeUsername: "horgan.madore",
      creatorUsername: "jerrie.mcginnis",
      id: user5.id,
      name: user5.firstName,
      storyPoints: user5.storyPoints
    },
    {
      assigneeUsername: "biff.wellington",
      creatorUsername: "becky.hawkins",
      id: user3.id,
      name: user3.firstName,
      storyPoints: user3.storyPoints
    }
  ]
};
