
"use strict";

var r = require('rethinkdbdash')({
  host: 'rethinkdb'
});

module.exports = () => {

return r.tableCreate("User").run()
    .catch(() => {
        // ignore as probably User table already exists
    })
    .then(() => {
        return r.table("User").delete().run();
    })
    .then(() => {

        const users = [
          {
            id: "b808972a-ec28-4d69-a194-d1e05c5ff0e8",
            username: "mina.hernadi",
            firstName: "Mina",
            lastName: "Hernadi",
            age: 20,
            tags: [
              "da2f64bd-1d70-4415-a232-95b2f35366ba",
              "3f2213f2-0c04-44bf-907a-9f167404adff",
            ],
            roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
            createdAt: new Date("2015-08-22T22:24:15.000Z"),
            updatedAt: null
          },
          {
            id: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
            username: "horgan.madore",
            firstName: "Horgan",
            lastName: "Madore",
            age: 26,
            tags: [
              "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8",
              "a239fb8d-a632-4f6e-91b1-16cbaa519800",
            ],
            roleId: "6eeac08b-818b-480f-896f-8e4ea3f57267",
            createdAt: new Date("2015-08-22T07:42:47.000Z"),
            updatedAt: null
          },
          {
            id: "d7885ec6-ece3-427a-b15c-c114d27d2f82",
            username: "jerrie.mcginnis",
            firstName: "Jerrie",
            lastName: "Mcginnis",
            age: 23,
            tags: [
              "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8",
              "72db1cef-4e21-4946-a16e-1e0690d69dc7",
            ],
            roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
            createdAt: new Date("2015-08-22T07:34:11.000Z"),
            updatedAt: null
          },
          {
            id: "4c374646-ff38-4a8b-adf6-0bf0e3723c75",
            username: "kristy.daniel",
            firstName: "Kristy",
            lastName: "Daniel",
            age: 17,
            tags: [],
            roleId: "6eeac08b-818b-480f-896f-8e4ea3f57267",
            createdAt: new Date("2015-08-22T05:34:12.000Z"),
            updatedAt: null
          },
          {
            id: "ede8e906-03f4-40e8-9b8e-46c1aad72de6",
            username: "jardetzky.gallucio",
            firstName: "Jardetzky",
            lastName: "Gallucio",
            age: 35,
            tags: [
              "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71",
            ],
            roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
            createdAt: new Date("2015-08-22T07:34:11.000Z"),
            updatedAt: null
          }
        ];

        return r.table("User").insert(users).run();
    })
    .then(() => {
        return r.tableCreate("AclRole").run();
    })
    .catch(() => {
        // ignore as probably User table already exists
    })
    .then(() => {
        return r.table("AclRole").delete().run();
    })
    .then(() => {

        const aclRoles = [
          {
            id: "6eeac08b-818b-480f-896f-8e4ea3f57267",
            name: "admin",
            description: "Application administrator",
            parent: null,
            status: "active",
            createdAt: new Date("2015-08-22T23:12:31.000Z"),
            updatedAt: null
          },
          {
            id: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
            name: "user",
            description: "Application user",
            parent: "d16db438-35a9-4dcc-946c-c2460aa7dda7",
            status: "active",
            createdAt: new Date("2015-08-22T12:17:02.000Z"),
            updatedAt: null
          }
        ];

        return r.table("AclRole").insert(aclRoles).run();
    })
    .then(() => {
        return r.tableCreate("Tag").run();
    })
    .catch(() => {
        // ignore as probably User table already exists
    })
    .then(() => {
        return r.table("Tag").delete().run();
    })
    .then(() => {

        const tags = [
          {
            id: "da2f64bd-1d70-4415-a232-95b2f35366ba",
            name: "Tag A",
            status: "active",
            createdAt: new Date("2015-08-22T21:34:32.000Z"),
            updatedAt: null
          },
          {
            id: "3f2213f2-0c04-44bf-907a-9f167404adff",
            name: "Tag B",
            status: "active",
            createdAt: new Date("2015-08-22T12:17:02.000Z"),
            updatedAt: null
          },

          {
            id: "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8",
            name: "Tag C",
            status: "active",
            createdAt: new Date("2015-08-22T21:34:32.000Z"),
            updatedAt: null
          },
          {
            id: "a239fb8d-a632-4f6e-91b1-16cbaa519800",
            name: "Tag D",
            status: "active",
            createdAt: new Date("2015-08-22T12:17:02.000Z"),
            updatedAt: null
          },
          {
            id: "72db1cef-4e21-4946-a16e-1e0690d69dc7",
            name: "Tag E",
            status: "active",
            createdAt: new Date("2015-08-22T12:17:02.000Z"),
            updatedAt: null
          },
          {
            id: "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71",
            name: "Tag F",
            status: "active",
            createdAt: new Date("2015-08-22T12:17:02.000Z"),
            updatedAt: null
          }
        ];

        return r.table("Tag").insert(tags).run();
    })
    .then(() => {
        return r.tableCreate("Post").run();
    })
    .catch(() => {
        // ignore as probably User table already exists
    })
    .then(() => {
        return r.table("Post").delete().run();
    })
    .then(() => {

        const posts = [
            {
                id: "f65a5fe6-371a-4216-884a-ab61047ab3db",
                name: "Install Grunt Watch",
                description: "create a Gruntfile...",
                status: "active",
                authorId: "b808972a-ec28-4d69-a194-d1e05c5ff0e8",
                createdAt: new Date("2015-08-22T21:34:32.000Z"),
                updatedAt: null
            },
            {
                id: "61ad71eb-0ca2-49af-b371-c16599207bbc",
                name: "Concatenate files...",
                description: "connect multiple files",
                status: "active",
                authorId: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
                createdAt: new Date("2015-08-22T21:34:32.000Z"),
                updatedAt: null
            },
            {
                id: "fbf750db-8d63-446e-ba8b-603f2d40c35d",
                name: "Compress CSS files",
                description: "To compress files...",
                status: "active",
                authorId: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
                createdAt: new Date("2015-08-22T21:34:32.000Z"),
                updatedAt: null
            },
            {
                id: "4b9c8c26-9962-4351-b7f8-2e6e333c9ff5",
                name: "Minify images",
                description: "To minify files...",
                status: "active",
                userId: "4c374646-ff38-4a8b-adf6-0bf0e3723c75",
                createdAt: new Date("2015-08-22T21:34:32.000Z"),
                updatedAt: null
            },
            {
                id: "563655d2-be47-4efd-b2e7-ebc261d383d7",
                name: "Lint .scss files",
                description: "lint scss files...",
                status: "active",
                userId: "ede8e906-03f4-40e8-9b8e-46c1aad72de6",
                createdAt: new Date("2015-08-22T21:34:32.000Z"),
                updatedAt: null
            }
        ];

        return r.table("Post").insert(posts).run();
    });
};
