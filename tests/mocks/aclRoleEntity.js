
"use strict";

const uuid = require("uuid");

module.exports = {
  id: {type: "string", optional: false, def: uuid.v4()},
  name: {"type": 'string'},
  description: {"type": 'string'},
  parent: {"type": 'string'},      
  status: {type: "string", optional: false, def: "active"},
  createdAt: {type: "date", optional: false, def: new Date()},
  updatedAt: {type: "date", optional: false, def: null}
};