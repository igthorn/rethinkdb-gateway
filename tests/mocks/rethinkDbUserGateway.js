
"use strict";

const appRootPath      = require("app-root-path");
const _                = require("underscore");
const userEntity       = require("./../mocks/userEntity");
const aclRoleEntity    = require("./../mocks/aclRoleEntity");
const tagEntity        = require("./../mocks/tagEntity");
const postEntity       = require("./../mocks/postEntity");
const RethinkDbGateway = require(appRootPath + "/lib/rethinkDbGateway");
const GatewayError     = require(appRootPath + "/lib/gatewayError");

class RethinkDbUserGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object dataProvider Instance of rethinkdbdash
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "User",
      "alias": "u"
    };

    /**
     * Object describing user"s relation
     *
     * @type Object
     */
    this.relations = {
      "role": {
        "table": "AclRole",
        "localColumn": "roleId",
        "targetColumn": "id",
        "defaultAlias": "r"
      },
      "tags": {
        "table": "Tag",
        "localColumn": "tags",
        "targetColumn": "id",
        "defaultAlias": "t"
      },
      "posts": {
        "localColumn": "id",
        "targetColumn": "authorId",
        "defaultAlias": "p"
      }
    };
  }

  /**
   * Method fetches all records matching passed request"s criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise promise Promise of DB result
   */
  fetchAll(request)
  {
    const entities = [userEntity];

    const joins = request.getJoins();

    if (_.contains(joins, "role")) {
      entities.push(aclRoleEntity);
    }

    if (_.contains(joins, "tags")) {
      entities.push(tagEntity);
    }

    if (_.contains(joins, "posts")) {
      entities.push(postsEntity);
    }

    return this.query(request, entities);
  }

  /**
   * Method inserts new entity to table
   *
   * @param User user User entity
   * @return Promise user Promise of newly created user entity
   */
  insert(user)
  {
    return this.validateUsername(user)
      .then(super.insert.bind(this));
  }

  /**
   * Helper method validates username to be inserted
   *
   * @param User user User entity
   * @return Promise user Promise of exception if username exists
   * otherwise promise of passed object
   */
  validateUsername(user)
  {
    return this.dataProvider.table(this.table.name)
      .getAll(user.get("username"), {index: "username"})
      .then((users) => {
        if (!_.isEmpty(users)) {
          throw new GatewayError(
            "Username already in use"
          );
        }

        return user;
      });
  }
}

module.exports = RethinkDbUserGateway;
