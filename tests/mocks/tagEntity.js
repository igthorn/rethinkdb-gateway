
"use strict";

const uuid = require("uuid");

module.exports = {
  id: {type: "string", optional: false, def: uuid.v4()},
  name: {type: "string"},
  status: {type: "string", optional: false, def: "active"},
  createdAt: {type: "date", optional: false, def: new Date()},
  updatedAt: {type: ["date", "null"], optional: false, def: null}
};